# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class NgoGovItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass

class NGO(scrapy.Item):
	ngo_id = scrapy.Field()
	name = scrapy.Field()
	chairman = scrapy.Field()
	secretary = scrapy.Field()
	parent_org = scrapy.Field()

	state = scrapy.Field()
	city = scrapy.Field()
	email = scrapy.Field()
	telephone = scrapy.Field()
	address = scrapy.Field()
	mobile_number = scrapy.Field()

