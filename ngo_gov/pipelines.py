# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

from scrapy.exceptions import DropItem
import MySQLdb

# Move to settings.py file


"""
DDL

create table ngo_details ( 

	ngo_id VARCHAR(100) ,
	name VARCHAR(100) ,
	chairman VARCHAR(100) ,
	secretary VARCHAR(100) ,
	parent_org VARCHAR(100) ,
	state VARCHAR(100) ,
	city VARCHAR(100) ,
	email VARCHAR(100) ,
	telephone VARCHAR(100) ,
	address VARCHAR(100) ,
	mobile_number VARCHAR(100)

	)

"""
class NgoGovPipeline(object):

	db = "ngo_app"
	user = "root"
	passwd = "root"
	host = "localhost"
    
	def __init__(self):
		# Move to spider open function
		self.db = MySQLdb.connect(host=self.host,user=self.user,passwd=self.passwd,db=self.db)


	def process_item(self, item, spider):
		try:
			cur = self.db.cursor()

			query = """INSERT INTO ngo_details VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}')"""

			cur.execute(
				query.format(
						item['ngo_id'],item['name'],
						item['chairman'],item['secretary'],
						item['parent_org'],
						item['state'],item['city'],
						item['email'],item['telephone'],
						item['address'],item['mobile_number']
					))
		except:
			print "Duplicate entry for {0}".format(item['ngo_id'])
		# Move to spider close function 
		self.db.commit()

		return item