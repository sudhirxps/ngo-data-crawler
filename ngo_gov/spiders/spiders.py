import scrapy
from bs4 import BeautifulSoup
from scrapy.http import FormRequest
from ngo_gov.items import NGO
class DmozSpider(scrapy.Spider):
    name = "ngo"
    allowed_domains = ["ngo.india.gov.in"]
    start_urls = [
       "http://ngo.india.gov.in/ngo_stateschemes_ngo.php"
    ]

    # Parse the intial page to extract states and start scraping individual state list
    def parse(self, response):

        bs = BeautifulSoup(response.body,"html.parser")

        # Extract all hyperlinks in the page
        hrefs = bs.findAll('a',{'class':'bluelink11px'})
        
        for href in hrefs:
            
    
            # State keys are located in the href attribute of hyperlinks

            if "javascript:sect_key" in href['href']:

                state_key = href['href'].split("('")[-1].split("')")[0]

                url = "http://ngo.india.gov.in/state_ngolist_ngo.php?records=200&state_value=" + state_key
                
                yield scrapy.Request(url, callback=self.parse_ngo_list_page)


    def parse_ngo_list_page(self,response):

        bs = BeautifulSoup(response.body,"html.parser")
        # Extract all hyperlinks in the page
        hrefs = bs.findAll('a',{'class':'bluelink11px'})

        ngos = []
        for href in hrefs:

            if "javascript:view_ngo" in href['href']:

                function_arguments = href['href'].split('javascript:view_ngo(')[-1]
                ngo_id = function_arguments.split(',')[0].replace("'","")
                
                url = "http://ngo.india.gov.in/view_ngo_details_ngo.php"

                data = {
                    'ngo_id':ngo_id
                }

                
                yield FormRequest(url=url,
                               formdata=data,
                               callback=self.parse_ngo_detail_page,
                              )

        # Pagination URLS

        # Extract all hyperlinks in the page
        hrefs = bs.findAll('a')

        for href in hrefs:
            # Check if the anchor tags are of paginations type
            if "page=" in href['href']:
                url = href['href']
                yield scrapy.Request(url, callback=self.parse_ngo_list_page)


    """
    Crawl the detail page of each NGO
    TODO Add more details 
    """
    def parse_ngo_detail_page(self, response):
            

        bs = BeautifulSoup(response.body,"html.parser")

        form_parent = bs.find('form',{'id':'frm_admlog'})

        table = form_parent.find('table').find('table')

        trs = table.findAll('tr')

        print len(trs)
        information = {}

        for i in range(0,len(trs)):

            tds = trs[i].findAll('td')

            if len(tds) == 4:
                information[tds[1].text.strip()] = tds[3].text
            elif len(tds) == 2:
                information['Name'] = tds[0].text.split(':')[-1].strip()
                        
        print information.keys()
        print information[u'State']

        item = NGO()
        item['ngo_id'] = response.headers.get('ngo_id')
        item['reg_id'] = information['Registration No']
        try:

            item['name'] = information['Name']
            item['chairman'] = information['Chairman']
            item['secretary'] = information['Secretary']
            item['parent_org'] = information['Umbrella/Parent Organization']
            item['state'] = information['State']
            item['city'] = information['City']
            item['email'] = information['E-mail']
            item['telephone'] = information['Telephone']
            item['address'] = information['Address']
            item['mobile_number'] = information['Mobile No']

        except:
            pass
            
        yield item
